FROM openjdk:11-slim as builder
ENV APP_HOME /usr/app/
WORKDIR $APP_HOME
COPY gradle ./gradle
COPY docker.build.gradle ./build.gradle
COPY settings.gradle gradlew ./
RUN ./gradlew getDeps
COPY . .
RUN ./gradlew build -x test

FROM openjdk:11-slim as runtime

MAINTAINER Renan Calmon

ENV JAVA_OPTS=""
ENV APP_HOME=/usr/app

RUN mkdir $APP_HOME
RUN mkdir $APP_HOME/config
RUN mkdir $APP_HOME/logs

WORKDIR $APP_HOME
COPY --from=builder $APP_HOME/build/libs/api-rest-sw-0.0.1-SNAPSHOT.jar app.jar
EXPOSE 8090
CMD java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar app.jar