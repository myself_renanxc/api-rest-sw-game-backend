package com.b2w.apirestsw.v1.api;

import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

import com.b2w.apirestsw.api.v1.PlanetController;
import com.b2w.apirestsw.api.v1.ResponseModel;
import com.b2w.apirestsw.api.v1.resource.PlanetResourceAssembler;
import com.b2w.apirestsw.api.v1.resource.PlanetResourceRequest;
import com.b2w.apirestsw.api.v1.resource.PlanetResourceResponse;
import com.b2w.apirestsw.domain.orm.Planet;
import com.b2w.apirestsw.service.PlanetService;
import com.b2w.apirestsw.service.StarWarsApiService;

public class PlanetControllerUnitTest {

	PlanetController controller;
	
	@Mock
	PlanetService planetService;
	@Mock
	StarWarsApiService swService;
	@Mock
	PlanetResourceAssembler planetAssembler;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		controller = new PlanetController(planetService, planetAssembler, swService);	
	}

	@Test
	public void testa_criar_planeta_com_parametros_corretos() {
		Planet planeta = new Planet("Yavin IV", "temperate, tropical", "jungle, rainforests");
		PlanetResourceRequest planetaReq = new PlanetResourceRequest("Yavin IV", "temperate, tropical", "jungle, rainforests");
		PlanetResourceResponse planetaRes = new PlanetResourceResponse();
		planetaRes.setIdPlanet("ID");
		planetaRes.setName("Yavin IV");
		planetaRes.setClimate("temperate, tropical");
		planetaRes.setGround("jungle, rainforests");
		planetaRes.setAppearance(5);

		when(planetAssembler.toDomain(planetaReq)).thenReturn(planeta);
		when(planetService.add(planeta)).thenReturn(planeta);
		when(planetAssembler.toResource(planeta)).thenReturn(planetaRes);
		
		ResponseEntity<ResponseModel> response = controller.addAPlanet(planetaReq);

		Assert.assertEquals(200, response.getStatusCodeValue());
	}
	
	@Test
	public void testa_criar_planeta_com_parametros_incorretos() {
		Planet planeta = new Planet("Yavin IV", "temperate, tropical", "jungle, rainforests");
		PlanetResourceRequest planetaRes = new PlanetResourceRequest("a","b","c");
		
		when(planetService.add(planeta)).thenReturn(planeta);
		when(planetAssembler.toDomain(planetaRes)).thenReturn(planeta);
		
		ResponseEntity<ResponseModel> response = controller.addAPlanet(planetaRes);
				
		Assert.assertEquals(422, response.getStatusCodeValue());
	}

}