package com.b2w.apirestsw.services;

import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.b2w.apirestsw.domain.orm.Planet;
import com.b2w.apirestsw.domain.repository.PlanetRepository;
import com.b2w.apirestsw.service.PlanetServiceImpl;


public class PlanetaServiceUnitTest {

	@Mock
	private PlanetRepository repository;
	
	private PlanetServiceImpl service;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		service = new PlanetServiceImpl(repository);
	}
	
	@Test
	public void testa_adicionar_planeta_corretamente() {
		Planet planeta = new Planet("a","b","c");
		planeta.setId("AAA1");
		when(repository.save(planeta)).thenReturn(planeta);
		
		Planet response = service.add(planeta);
		
		Assert.assertEquals(planeta.getId(), response.getId());
	}
	
	@Test
	public void testa_adicionar_planeta_incorretamente() {
		Planet planeta = new Planet("a","b","c");
		planeta.setId("AAA1");
		when(repository.save(planeta)).thenReturn(new Planet());
	
		Planet response = service.add(planeta);
		
		Assert.assertNotEquals(planeta.getId(), response.getId());
	}

}