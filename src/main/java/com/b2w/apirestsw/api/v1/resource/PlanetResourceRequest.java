package com.b2w.apirestsw.api.v1.resource;


import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.AllArgsConstructor;
import lombok.Getter;

@JsonPropertyOrder({ "nome", "clima", "terreno"})
@Getter
@AllArgsConstructor
public class PlanetResourceRequest {

	@NotNull
	@JsonProperty("nome")
	private String name;
	@NotNull
	@JsonProperty("clima")
	private String climate;
	@NotNull
	@JsonProperty("terreno")
	private String ground;
}