package com.b2w.apirestsw.api.v1.resource;


import org.springframework.hateoas.ResourceSupport;
import org.springframework.hateoas.core.Relation;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Getter;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@JsonPropertyOrder({ "id", "nome", "clima", "terreno", "aparicoes" })
@Relation(value="planet", collectionRelation="planets")
public class PlanetResourceResponse extends ResourceSupport {

	@JsonProperty("id")
	private String idPlanet;

	@JsonProperty("nome")
	private String name;
	@JsonProperty("clima")
	private String climate;
	@JsonProperty("terreno")
	private String ground;
	@JsonProperty("aparicoes")
	private int appearance;
	
	public void setIdPlanet(String idPlanet) {
		this.idPlanet = idPlanet;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setClimate(String climate) {
		this.climate = climate;
	}
	public void setGround(String ground) {
		this.ground = ground;
	}
	public void setAppearance(int appearance) {
		this.appearance = appearance;
	}
}