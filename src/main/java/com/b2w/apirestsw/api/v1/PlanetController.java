package com.b2w.apirestsw.api.v1;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.b2w.apirestsw.api.v1.resource.PlanetResourceAssembler;
import com.b2w.apirestsw.api.v1.resource.PlanetResourceRequest;
import com.b2w.apirestsw.api.v1.resource.PlanetResourceResponse;
import com.b2w.apirestsw.domain.dto.StarWarsApiDTO;
import com.b2w.apirestsw.service.PlanetService;
import com.b2w.apirestsw.service.StarWarsApiService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.AllArgsConstructor;

@AllArgsConstructor
@RestController
@CrossOrigin()
@RequestMapping("/v1/planeta")
public class PlanetController {

	@Autowired
	private PlanetService planetService;
	@Autowired
	private PlanetResourceAssembler planetAssembler;
	@Autowired
	private StarWarsApiService starWarsApiService;
	
	private static final Logger logger = LogManager.getLogger(PlanetController.class);
	
	@ApiOperation(
			value="Cadastrar um novo planeta", 
			response=ResponseModel.class, 
			notes="Essa opera��o salva um novo registro com as informa��es de planeta.")
	@ApiResponses(value= {
			@ApiResponse(
					code=200, 
					message="Retorna um ResponseModel com uma mensagem de sucesso",
					response=ResponseModel.class
					)
	})
	@PostMapping
	public @ResponseBody ResponseEntity<ResponseModel> addAPlanet(@Valid @RequestBody PlanetResourceRequest planetRequest){
		try {
			logger.debug("Um Planeta est� sendo inserido - Planeta: {}", () -> planetRequest);

			PlanetResourceResponse planet = planetAssembler.toResource(
				planetService.add(
					planetAssembler.toDomain(
						planetRequest
					)
				)
			);
			
			logger.debug("O Planeta foi inserido com sucesso - Planeta: {}", () -> planet);

			return new ResponseEntity<ResponseModel>(
				new ResponseModel(
					HttpStatus.CREATED.value(),
					"Registro salvo com sucesso!"
				),
				PlanetResourceAssembler.createHeader(
					"Location",
					planet.getIdPlanet()
				), 
				HttpStatus.OK
			);
		} catch (Exception e){
			logger.error("Erro ao inserir Planeta");
			
			return new ResponseEntity<ResponseModel>(
				new ResponseModel(
					HttpStatus.UNPROCESSABLE_ENTITY.value(),
					e.getMessage()
				),
				HttpStatus.UNPROCESSABLE_ENTITY
			);	
		}
	}
	
	@ApiOperation(
			value="Listar todos os planetas", 
			response=PlanetResourceResponse.class, 
			notes="Essa opera��o obtem todos os planetas.")
	@ApiResponses(value= {
			@ApiResponse(
				code=200, 
				message="Retorna uma lista de planetas",
				response=PlanetResourceResponse.class
			)
	})
	@GetMapping
	public @ResponseBody ResponseEntity<Resources<PlanetResourceResponse>> listPlanets() {
		logger.debug("Buscando todos os Planetas");

		List<PlanetResourceResponse> planets = planetService.findAll()
				.stream()
				.map(planetAssembler::toResource)
				.collect(Collectors.toList());
		for( PlanetResourceResponse planet : planets) {
			String name = planet.getName();
			StarWarsApiDTO starWarsApiDTO = starWarsApiService.findStarWarsApi(name);
			planet.setAppearance(
				starWarsApiService.countAppearance(starWarsApiDTO, name)
			);
		};
		
		logger.debug("Planetas encontrados: {}", () -> planets);
		
		return new ResponseEntity<Resources<PlanetResourceResponse>>(
			planetAssembler.toAllResources(planets),
			HttpStatus.OK
		);
	
	}
	
	@ApiOperation(
			value="Busca planetas por nome", 
			response=PlanetResourceResponse.class, 
			notes="Essa opera��o obtem todos os planetas que contenham o nome da path.")
	@ApiResponses(value= {
			@ApiResponse(
					code=200, 
					message="Retorna uma lista de planetas",
					response=PlanetResourceResponse.class
					)
	})
	@GetMapping("/nome/{nome}") 
	public @ResponseBody ResponseEntity<Resources<PlanetResourceResponse>> findPlanetByName(@Valid @PathVariable("nome") String name) {
		logger.debug("Buscando planetas por nome: {}", () -> name);
		
		List<PlanetResourceResponse> planets = planetService.findByName(name)
				.stream()
				.map(planetAssembler::toResource)
				.collect(Collectors.toList());
		
		for( PlanetResourceResponse planet: planets) {
			StarWarsApiDTO starWarsApiDTO = starWarsApiService.findStarWarsApi(name);
			planet.setAppearance(
				starWarsApiService.countAppearance(starWarsApiDTO, name)
			);
		};
		
		logger.debug("Planetas encontrados: {}", () -> name);
		
		return new ResponseEntity<Resources<PlanetResourceResponse>>(
			planetAssembler.toNameResources(planets, name),
			HttpStatus.OK
		);
	}
	
	@ApiOperation(
			value="Busca planeta por id", 
			response=PlanetResourceResponse.class, 
			notes="Essa opera��o obtem o planeta que contenha o id da path.")
	@ApiResponses(value= {
			@ApiResponse(
					code=200, 
					message="Retorna uma planeta",
					response=PlanetResourceResponse.class
					)
	})
	@ResponseStatus(HttpStatus.OK)
	@GetMapping("/{id}") 
	public @ResponseBody ResponseEntity<PlanetResourceResponse> findPlanetById(@Valid @PathVariable("id") String planetId) {
		
		logger.debug("Buscando planeta id: {}", () -> planetId);

		PlanetResourceResponse planet = planetAssembler.toResource(
			planetService.findById(planetId)
		);
		
		String name = planet.getName();
		StarWarsApiDTO starWarsApiDTO = starWarsApiService.findStarWarsApi(name);
		planet.setAppearance(
			starWarsApiService.countAppearance(starWarsApiDTO, name)
		);
		
		PlanetResourceAssembler.addNameRel(planet);

		logger.debug("Planeta encontrado: {}", () -> planet);

		
		return new ResponseEntity<PlanetResourceResponse>(
			planet,
			HttpStatus.OK
		);
	}
	
	@ApiOperation(
			value="Remover planeta por id", 
			response=ResponseModel.class, 
			notes="Essa opera��o remove um registro com as informa��es de planeta.")
	@ApiResponses(value= {
			@ApiResponse(
					code=200, 
					message="Retorna um ResponseModel com uma mensagem de sucesso",
					response=ResponseModel.class
					)
	})
	@DeleteMapping("/{id}")
	public @ResponseBody ResponseEntity<ResponseModel> deleteAPlanet(@PathVariable("id") String id) {
		try {
			logger.debug("Deletando planeta pelo id: {}", () -> id);

			planetService.delete(id);
			
			logger.debug("Planeta deletado com sucesso");

			return new ResponseEntity<ResponseModel>(
				new ResponseModel(
					200,
					"Registro Deletado com sucesso!"
				),
				HttpStatus.ACCEPTED
			);
		} catch (Exception e){
			logger.debug("Erro ao deletar planeta: {}", () -> e.getMessage());
			
			return new ResponseEntity<ResponseModel>(
				new ResponseModel(
					500,
					e.getMessage()
				),
				HttpStatus.INTERNAL_SERVER_ERROR
			);
		}
	}

}
