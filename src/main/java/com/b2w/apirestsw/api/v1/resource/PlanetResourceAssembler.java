package com.b2w.apirestsw.api.v1.resource;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.List;

import org.springframework.hateoas.Resources;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;

import com.b2w.apirestsw.api.v1.PlanetController;
import com.b2w.apirestsw.domain.orm.Planet;

@Component
public class PlanetResourceAssembler extends ResourceAssemblerSupport<Planet, PlanetResourceResponse> {
	
    public PlanetResourceAssembler() {
        super(PlanetController.class, PlanetResourceResponse.class);
    }

    public Resources<PlanetResourceResponse> toAllResources(List<PlanetResourceResponse> planets) {
    	return new Resources<>(planets,linkTo(
			methodOn(PlanetController.class).listPlanets()
		).withSelfRel());
    }
    
    public Resources<PlanetResourceResponse> toNameResources(List<PlanetResourceResponse> planets, String name) {
    	return new Resources<>(planets,linkTo(
			methodOn(PlanetController.class).findPlanetByName(name)
		).withSelfRel());
    }
    
    @Override
    public PlanetResourceResponse toResource(Planet planet) {
    	PlanetResourceResponse planetResource = createResourceWithId(planet.getId(), planet);
        planetResource.setIdPlanet(planet.getId());
        planetResource.setName(planet.getName());
        planetResource.setClimate(planet.getClimate());
        planetResource.setGround(planet.getGround());
        return planetResource;
    }
    
	public Planet toDomain(PlanetResourceRequest planetResource) {
        return new Planet(
                planetResource.getName(),
                planetResource.getClimate(),
                planetResource.getGround()
		);
    }

	public static MultiValueMap<String, String> createHeader(String headerType, String id) {
    	MultiValueMap<String, String> header = new HttpHeaders();
    	header.add(headerType, linkTo(PlanetController.class).toString() + "/" + id);
    	return header;
    }

    public static void addNameRel(PlanetResourceResponse planetResource) {
        planetResource.add(
        		linkTo(methodOn(PlanetController.class)
        				.findPlanetByName(
        						planetResource.getName()
        				)
        		).withRel("sameName")
        );
    }
}