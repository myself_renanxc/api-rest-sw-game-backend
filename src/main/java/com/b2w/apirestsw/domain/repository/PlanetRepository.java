package com.b2w.apirestsw.domain.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.b2w.apirestsw.domain.orm.Planet;

@Repository
public interface PlanetRepository extends MongoRepository<Planet, String> {

	@Query("{name:?0}")
	public Optional<List<Planet>> findByName(String name);
}
