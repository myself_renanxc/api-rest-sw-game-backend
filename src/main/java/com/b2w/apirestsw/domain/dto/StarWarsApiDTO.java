package com.b2w.apirestsw.domain.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class StarWarsApiDTO {
	List<StarWarsApiResultDTO> results;
}
