package com.b2w.apirestsw.domain.orm;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import com.mongodb.lang.NonNull;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Document(collection = "planets")
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Planet {

	@Id
	private String id;

	@Indexed(unique = true)
	@NonNull
	private String name;
	@NonNull
	private String climate;
	@NonNull
	private String ground;

	public Planet(String name, String climate, String ground) {
		this.name = name;
		this.climate = climate;
		this.ground = ground;
	}
}