package com.b2w.apirestsw.service;

import java.util.List;
import com.b2w.apirestsw.domain.orm.Planet;

public interface PlanetService {

	Planet add(Planet planet);
	List<Planet> findAll();
	List<Planet> findByName(String nome);
	Planet findById(String id);
	void delete(String id);
}