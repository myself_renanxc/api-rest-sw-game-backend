package com.b2w.apirestsw.service;

import com.b2w.apirestsw.domain.dto.StarWarsApiDTO;

public interface StarWarsApiService {
	
	StarWarsApiDTO findStarWarsApi(String nome);
	Integer countAppearance(StarWarsApiDTO starWarsApiDTO, String name);
}
