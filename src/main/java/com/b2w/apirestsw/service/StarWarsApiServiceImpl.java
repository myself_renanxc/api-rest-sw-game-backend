package com.b2w.apirestsw.service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.b2w.apirestsw.domain.dto.StarWarsApiDTO;
import com.b2w.apirestsw.domain.dto.StarWarsApiResultDTO;

@Service
public class StarWarsApiServiceImpl implements StarWarsApiService {

	@Value("${starwars.api.url}")
	private String starWarsAPIurl;
	@Autowired
	private RestTemplate rest;
	protected static final Logger LOGGER = LoggerFactory.getLogger(StarWarsApiServiceImpl.class);

	public Integer countAppearance(StarWarsApiDTO starWarsApi, String name) {
		if(starWarsApi.getResults().isEmpty()) {
			LOGGER.info("Planeta n�o encontrado");
			return 0;
		} else {
			List<String> films = starWarsApi.getResults().stream()
					.filter( 
						planet-> planet.getName().equals(name)
					).findFirst().or(() -> Optional.of(new StarWarsApiResultDTO()) )
					.get().getFilms();
			
			return (Objects.nonNull(films)) 
					? films.size() 
					: 0;
		}
	}
	@Cacheable(value="starWarsApiCache", key="#p0")
	public StarWarsApiDTO findStarWarsApi(String name) {
		try {
			String url = getUrl(name);
			return rest.getForObject(url, StarWarsApiDTO.class);			
		}
		catch (Exception e) {
			LOGGER.error("Erro ao pesquisar apari��es: " + e.getMessage());
			throw new InternalError();
		}
	}

	private String getUrl(String name) {
		return starWarsAPIurl + "?search=" + name;
	}
}
