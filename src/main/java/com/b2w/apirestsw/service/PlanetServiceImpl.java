package com.b2w.apirestsw.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.b2w.apirestsw.domain.orm.Planet;
import com.b2w.apirestsw.domain.repository.PlanetRepository;
import com.b2w.apirestsw.exception.ResourceNotFoundException;

@Service
public class PlanetServiceImpl implements PlanetService {

	private PlanetRepository repository;
	
	@Autowired
	public PlanetServiceImpl(PlanetRepository repository) {
		this.repository = repository;
	}
	
	@Override
	public Planet add(Planet planet) {
		return repository.save(planet);
	}

	@Override
	public List<Planet> findAll() {
		List<Planet> planets = repository
				.findAll();
		return planets;
	}

	@Override
	@Cacheable(value="planetCache", key="#p0")
	public List<Planet> findByName(String name) {
		List<Planet> planets = repository
				.findByName(name).orElseThrow(() -> new ResourceNotFoundException());
		return planets;
	}

	@Override
	@Cacheable(value="planetCache", key="#p0")
	public Planet findById(String id) {
		return repository
				.findById(id).orElseThrow(() -> new ResourceNotFoundException());
	}

	@Override
	@CacheEvict(value="planetCache", key="#p0")
	public void delete(String id) {
		Planet planet = repository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException());
		repository.delete(planet);
	}
}