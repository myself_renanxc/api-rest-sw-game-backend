package com.b2w.apirestsw;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@RequestMapping("/")
@SpringBootApplication
@EnableCaching
public class ApiRestSwApplication{

	public static void main(String[] args) {
		SpringApplication.run(ApiRestSwApplication.class, args);
	}

	@GetMapping
	public @ResponseBody String HealthCheck() {
		return "A aplica��o est� rodando!";
	}
}
