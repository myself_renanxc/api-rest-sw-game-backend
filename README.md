# API Rest SW - Game Backend

##Set Up
Ap�s efetuar o 'git pull' do projeto para sua m�quina, dever� serguir alguns dos passos das op��es abaixo:

##Op��o 1: Docker-compose
	Com o Docker-compose instalado em sua m�quina:
		- docker-compose up
	Dentro do arquivo docker-compose.yml, poder� descomentar a linha em env para a imagem do arquivo app, assim conseguir� debugar do container direto para o Eclipse.
	Para isso precisar� configurar tamb�m no Eclipse em "Run > Debug configurations > Remote Java Application" e apontar para a porta 5005.
	
##Op��o 2: Build Gradle

	Ser� necess�rio installar as dependencias a partir do Gradle: (� preciso estar na raiz do projeto) Caso tenha o Gradle configurado no Path:
		- gradle build 
	Caso n�o possua, pode usar o wrapper que se encontra na raiz do projeto com o seguinte comando: [
		- ./gradlew build 
	Caso sinta-se mais confort�vel tamb�m � possivel usa-lo diretamente pelo eclipse

##Documenta��o
A documenta��o foi feita utilizando a lib Swagger, que dever� ser acessada como host:8090/swagger-ui.html.

##Testes Automatizados
Os testes podem ser encontrados em src/test/java e serem executados pelo comando gradle (ou ./gradlew) test

##Logs
Os logs de consulta poder�o ser visualizados na para ./logs